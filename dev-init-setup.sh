#!/bin/bash

# See http://sourabhbajaj.com/mac-setup and 
# https://www.taniarascia.com/setting-up-a-brand-new-mac-for-development/ 
# for more details

# Customize Finder
defaults write com.apple.finder AppleShowAllFiles YES
defaults write com.apple.finder ShowPathbar -bool true
defaults write com.apple.finder ShowStatusBar -bool true

# Install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install mas-cli (Mac App Store)
brew install mas

# Install app from Brewfile
brew bundle install

# Install nvm
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash
nvm install node
source ~/.zshrc
nvm use node

# Install Gulp globally
npm install --global gulp-cli

# Install ng-cli globally
npm install --global ng-cli

# Install Python
#brew install python
#pip install --upgrade setuptools
#pip install --upgrade pip
#brew install pyenv
#exec $SHELL
#pyenv install 2.7.12
#pyenv install 3.6.3
#pyenv global 2.7.12 3.6.3
#pyenv rehash
#pip install virtualenv


# Install Ruby Version Manager
#\curl -sSL https://get.rvm.io | bash -s stable
#rvm install ruby-head
#gem install bundler