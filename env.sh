#!/bin/zsh
# To include env.sh, open ~/.zshrc and add the following:
#
# source ~/code/config/env.sh

# Owner
export USER_NAME="???"
export PROJECT_HOME=$HOME/code

# PATH
export PATH="/usr/local/share/python:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
export JAVA_HOME="`/usr/libexec/java_home -v 1.8`"
export EDITOR='code -w'

### Functions ###

# FileSearch
function f() { find . -iname "*$1*" ${@:2} }
function r() { grep "$1" ${@:2} -R . }

#mkdir and cd
function mkcd() { mkdir -p "$@" && cd "$_"; }

### Aliases ###
alias cppcompile='c++ -std=c++11 -stdlib=libc++'

# Use sublimetext for editing config files
alias zshconfig="subl ~/.zshrc"
alias envconfig="subl ~$PROJECT_HOME/config/env.sh"

# Update Homebrew
alias brewup="brew update; brew upgrade; brew prune; brew cleanup; brew doctor"

# Show/hide all files in Finder
alias showf="defaults write com.apple.finder AppleShowAllFiles YES"
alias hidef="defaults write com.apple.finder AppleShowAllFiles NO"

### SOURCE ###

# Kubernetes pod autocomplete
source <(kubectl completion zsh)