# dev-init-setup-mac

Konfigurasjonsfiler for utviklingsmaskiner med OSX. Baserer seg i stor grad på Homebrew.

## Folderstruktur (forslag)

```
~/dev-init-setup.sh
~/Brewfile
~/code/config/env.sh
```

## Installering

```
vi env.sh # Legg inn ditt brukernavn
sh dev-init-setup.sh
```

Simples.

Docker må installeres "manuelt", siden jeg ikke var helt sikker på om brew installering var stabil.

SSH må settes opp manuelt, se kildene under.

## Kilder

Se disse guidene for detaljer

* http://sourabhbajaj.com/mac-setup
* https://www.taniarascia.com/setting-up-a-brand-new-mac-for-development/ 
